#+Title: Test Latex generated images

* Why
Because Latex can be used to make [[http://www.texample.net/tikz/examples/][beautiful illustrations]], and
org-mode [[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-LaTeX.html][allows to generated png or svg files]] from latex source
blocks.
* Trivial example
** Testing Latex to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:
 latex \rightarrow PNG
 #+HEADER: :file img/test-latex-img.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}")
 #+HEADER: :fit yes :imoutoptions -geometry 400 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports both
 \begin{tikzpicture}
 \draw[->] (-3,0) -- (-2,0) arc[radius=0.5cm,start angle=-180,end angle=0] (-1,0) -- (1,0) arc[radius=0.5cm,start angle=180,end angle=0] (2,0) -- (3,0);
 \filldraw (-1.5,0) circle[radius=1mm];
 \filldraw (1.5,0) circle[radius=1mm];
 \end{tikzpicture}
 #+END_src

** Testing Latex to SVG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-img.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}")
 #+HEADER: :fit yes :imoutoptions -geometry 400 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports both
 \begin{tikzpicture}
 \draw[->] (-3,0) -- (-2,0) arc[radius=0.5cm,start angle=-180,end angle=0] (-1,0) -- (1,0) arc[radius=0.5cm,start angle=180,end angle=0] (2,0) -- (3,0);
 \filldraw (-1.5,0) circle[radius=1mm];
 \filldraw (1.5,0) circle[radius=1mm];
 \end{tikzpicture} 
 #+END_src


* Linear programming illustration

** Testing LP illustration to PNG
 #+HEADER: :file img/test-latex-tikz-linear-programming.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usepackage{pgfplots}" "\\usepgfplotslibrary{fillbetween}")
 #+HEADER: :fit yes :imoutoptions -geometry 600 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
   \begin{tikzpicture}
     \begin{axis}
       [   domain=-5:45,
       mark=none,
       ymin=-5,
       ymax= 50,
       samples=500,
       enlargelimits=false
       ]

       \addplot[orange, name path=A] {40-x};
       \addplot[blue, name path=B] {119/2 -35/20*x};
       \addplot[cyan!50!gray, name path=C] {480/15-x/3};
       \addplot[black, name path=D] {0};

       \addplot[gray] fill between[of=A and D, soft clip={domain=0:34}];

       \addplot[white] fill between[of=A and C, soft clip={domain=0:34}];
       \addplot[white] fill between[of=B and A, soft clip={domain=26:50}];

       \node[orange] at (axis cs:10,40){$\text{hops}:4x_1 + 4x_2 \leq 160$};
       \node[blue] at (axis cs:15,45){$\text{malt}:35x_1 + 20x_2 \leq 1190$};
       \node[cyan!50!gray] at (axis cs:30,25){$\text{corn}:5x_1 + 15x_2 \leq 480$};

       \node at (axis cs:0,32){$(0,32)$};
       \node at (axis cs:12,28){$(12,28)$};
       \node at (axis cs:26,14){$(26,14)$};
       \node at (axis cs:34,0){$(34,0)$};

     \end{axis}
   \end{tikzpicture}
 #+END_src

** Testing LP illustration to SVG

 #+HEADER: :file img/test-latex-tikz-linear-programming.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usepackage{pgfplots}" "\\usepgfplotslibrary{fillbetween}")
 #+HEADER: :fit yes :imoutoptions -geometry 2000 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
   \begin{tikzpicture}
     \begin{axis}
       [   domain=-5:45,
       mark=none,
       ymin=-5,
       ymax= 50,
       samples=500,
       enlargelimits=false
       ]

       \addplot[orange, name path=A] {40-x};
       \addplot[blue, name path=B] {119/2 -35/20*x};
       \addplot[cyan!50!gray, name path=C] {480/15-x/3};
       \addplot[black, name path=D] {0};

       \addplot[gray] fill between[of=A and D, soft clip={domain=0:34}];

       \addplot[white] fill between[of=A and C, soft clip={domain=0:34}];
       \addplot[white] fill between[of=B and A, soft clip={domain=26:50}];

       \node[orange] at (axis cs:10,40){$\text{hops}:4x_1 + 4x_2 \leq 160$};
       \node[blue] at (axis cs:15,45){$\text{malt}:35x_1 + 20x_2 \leq 1190$};
       \node[cyan!50!gray] at (axis cs:30,25){$\text{corn}:5x_1 + 15x_2 \leq 480$};

       \node at (axis cs:0,32){$(0,32)$};
       \node at (axis cs:12,28){$(12,28)$};
       \node at (axis cs:26,14){$(26,14)$};
       \node at (axis cs:34,0){$(34,0)$};

     \end{axis}
   \end{tikzpicture}
 #+END_src

* Listing example
From [[http://www.texample.net/tikz/examples/tikz-listings/][The TikZ gallery]] by  Valeria Borodin.
** Testing TikZ listing to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:
a bit broken ☹


 #+HEADER: :file img/test-latex-tikz-listing.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{positioning}" "\\usepackage{listings}")
 #+HEADER: :fit yes :imoutoptions -geometry 800 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results

   % Author: Valeria Borodin
   \lstset{%
     frame            = tb,    % draw frame at top and bottom of code block
     tabsize          = 1,     % tab space width
     numbers          = left,  % display line numbers on the left
     framesep         = 3pt,   % expand outward
     framerule        = 0.4pt, % expand outward 
     commentstyle     = \color{Green},      % comment color
     keywordstyle     = \color{blue},       % keyword color
     stringstyle      = \color{DarkRed},    % string color
     backgroundcolor  = \color{white}, % backgroundcolor color
     showstringspaces = false,              % do not mark spaces in strings
   }
   \begin{lstlisting}[language = C++, numbers = none, escapechar = !,
       basicstyle = \ttfamily\bfseries, linewidth = .6\linewidth] 
    int!
      \tikz[remember picture] \node [] (a) {};
    !puissance!
      \tikz[remember picture] \node [] (b) {};
    !(int x,!
      \tikz[remember picture] \node [] (c){};
    !int n) { 

	int i, p = 1; !\tikz[remember picture] \node [] (d){};!           

	for (i = 1; i <= n; i++) 
	  p = p * x; !\tikz[remember picture] \node [inner xsep = 40pt] (e){};! 

	return p; !
	  \tikz[remember picture] \node [] (f){};!  
    }
   \end{lstlisting}
   \begin{tikzpicture}[remember picture, overlay,
       every edge/.append style = { ->, thick, >=stealth,
				     DimGray, dashed, line width = 1pt },
       every node/.append style = { align = center, minimum height = 10pt,
				    font = \bfseries, fill= green!20},
		     text width = 2.5cm ]
     \node [above left = .75cm and -.75 cm of a,text width = 2.2cm]
				(A) {return value type};
     \node [right = 0.25cm of A, text width = 1.9cm]
				(B) {function name};
     \node [right = 0.5cm of B] (C) {list of formal parameters};
     \node [right = 4.cm of d]  (D) {local variables declaration};
     \node [right = 2.cm of e]  (E) {instructions};
     \node [right = 5.cm of f]  (F) {instruction \texttt{\bfseries return}};  
     \draw (A.south) + (0, 0) coordinate(x1) edge (x1|-a.north);
     \draw (B.south) + (0, 0) coordinate(x2) edge (x2|-b.north);
     \draw (C.south) + (0, 0) coordinate(x3) edge (x3|-c.north);
     \draw (D.west) edge (d.east) ;
     \draw (E.west) edge (e.east) ;  
     \draw (F.west) edge (f.east) ;
   \end{tikzpicture} 
 #+END_src

** Testing TikZ listing to SVG :noexport:
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-listing.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{positioning}" "\\usepackage{listings}")
 #+HEADER: :fit yes :imoutoptions -geometry 400 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\documentclass[border={35pt 10pt 150pt 60pt},svgnames]{standalone}

   % Author: Valeria Borodin
   \lstset{%
     frame            = tb,    % draw frame at top and bottom of code block
     tabsize          = 1,     % tab space width
     numbers          = left,  % display line numbers on the left
     framesep         = 3pt,   % expand outward
     framerule        = 0.4pt, % expand outward 
     commentstyle     = \color{Green},      % comment color
     keywordstyle     = \color{blue},       % keyword color
     stringstyle      = \color{DarkRed},    % string color
     backgroundcolor  = \color{white}, % backgroundcolor color
     showstringspaces = false,              % do not mark spaces in strings
   }
   \begin{lstlisting}[language = C++, numbers = none, escapechar = !,
       basicstyle = \ttfamily\bfseries, linewidth = .6\linewidth] 
    int!
      \tikz[remember picture] \node [] (a) {};
    !puissance!
      \tikz[remember picture] \node [] (b) {};
    !(int x,!
      \tikz[remember picture] \node [] (c){};
    !int n) { 

	int i, p = 1; !\tikz[remember picture] \node [] (d){};!           

	for (i = 1; i <= n; i++) 
	  p = p * x; !\tikz[remember picture] \node [inner xsep = 40pt] (e){};! 

	return p; !
	  \tikz[remember picture] \node [] (f){};!  
    }
   \end{lstlisting}
   \begin{tikzpicture}[remember picture, overlay,
       every edge/.append style = { ->, thick, >=stealth,
				     DimGray, dashed, line width = 1pt },
       every node/.append style = { align = center, minimum height = 10pt,
				    font = \bfseries, fill= green!20},
		     text width = 2.5cm ]
     \node [above left = .75cm and -.75 cm of a,text width = 2.2cm]
				(A) {return value type};
     \node [right = 0.25cm of A, text width = 1.9cm]
				(B) {function name};
     \node [right = 0.5cm of B] (C) {list of formal parameters};
     \node [right = 4.cm of d]  (D) {local variables declaration};
     \node [right = 2.cm of e]  (E) {instructions};
     \node [right = 5.cm of f]  (F) {instruction \texttt{\bfseries return}};  
     \draw (A.south) + (0, 0) coordinate(x1) edge (x1|-a.north);
     \draw (B.south) + (0, 0) coordinate(x2) edge (x2|-b.north);
     \draw (C.south) + (0, 0) coordinate(x3) edge (x3|-c.north);
     \draw (D.west) edge (d.east) ;
     \draw (E.west) edge (e.east) ;  
     \draw (F.west) edge (f.east) ;
   \end{tikzpicture} 
 #+END_src

* Tree visualisation of an algorithm
[[http://www.texample.net/tikz/examples/merge-sort-recursion-tree/][Merge sort illustration from the TikZ gallery]] by Manuel Kirsch


** Testing TikZ Tree to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-tree.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{verbatim}" "\\usepackage[active,tightpage]{preview}" "\\usepackage{fancybox}" "\\usepackage{tikz}")
 #+HEADER: :fit yes :imoutoptions -geometry 800 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
      % \author{Manuel Kirsch}
   \ovalbox{
   \begin{tikzpicture}[level/.style={sibling distance=60mm/#1}]
   \node [circle,draw] (z){$n$}
     child {node [circle,draw] (a) {$\frac{n}{2}$}
       child {node [circle,draw] (b) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}
	   child {node [circle,draw] (d) {$\frac{n}{2^k}$}}
	   child {node [circle,draw] (e) {$\frac{n}{2^k}$}}
	 } 
	 child {node {$\vdots$}}
       }
       child {node [circle,draw] (g) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}}
	 child {node {$\vdots$}}
       }
     }
     child {node [circle,draw] (j) {$\frac{n}{2}$}
       child {node [circle,draw] (k) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}}
	 child {node {$\vdots$}}
       }
     child {node [circle,draw] (l) {$\frac{n}{2^2}$}
       child {node {$\vdots$}}
       child {node (c){$\vdots$}
	 child {node [circle,draw] (o) {$\frac{n}{2^k}$}}
	 child {node [circle,draw] (p) {$\frac{n}{2^k}$}
	   child [grow=right] {node (q) {$=$} edge from parent[draw=none]
	     child [grow=right] {node (q) {$O_{k = \lg n}(n)$} edge from parent[draw=none]
	       child [grow=up] {node (r) {$\vdots$} edge from parent[draw=none]
		 child [grow=up] {node (s) {$O_2(n)$} edge from parent[draw=none]
		   child [grow=up] {node (t) {$O_1(n)$} edge from parent[draw=none]
		     child [grow=up] {node (u) {$O_0(n)$} edge from parent[draw=none]}
		   }
		 }
	       }
	       child [grow=down] {node (v) {$O(n \cdot \lg n)$}edge from parent[draw=none]}
	     }
	   }
	 }
       }
     }
   };
   \path (a) -- (j) node [midway] {+};
   \path (b) -- (g) node [midway] {+};
   \path (k) -- (l) node [midway] {+};
   \path (k) -- (g) node [midway] {+};
   \path (d) -- (e) node [midway] {+};
   \path (o) -- (p) node [midway] {+};
   \path (o) -- (e) node (x) [midway] {$\cdots$}
     child [grow=down] {
       node (y) {$O\left(\displaystyle\sum_{i = 0}^k 2^i \cdot \frac{n}{2^i}\right)$}
       edge from parent[draw=none]
     };
   \path (q) -- (r) node [midway] {+};
   \path (s) -- (r) node [midway] {+};
   \path (s) -- (t) node [midway] {+};
   \path (s) -- (l) node [midway] {=};
   \path (t) -- (u) node [midway] {+};
   \path (z) -- (u) node [midway] {=};
   \path (j) -- (t) node [midway] {=};
   \path (y) -- (x) node [midway] {$\Downarrow$};
   \path (v) -- (y)
     node (w) [midway] {$O\left(\displaystyle\sum_{i = 0}^k n\right) = O(k \cdot n)$};
   \path (q) -- (v) node [midway] {=};
   \path (e) -- (x) node [midway] {+};
   \path (o) -- (x) node [midway] {+};
   \path (y) -- (w) node [midway] {$=$};
   \path (v) -- (w) node [midway] {$\Leftrightarrow$};
   \path (r) -- (c) node [midway] {$\cdots$};
   \end{tikzpicture}}
 #+END_src

** Testing TikZ Tree to SVG :noexport:
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-tree.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{verbatim}" "\\usepackage[active,tightpage]{preview}" "\\usepackage{fancybox}" "\\usepackage{tikz}")
 #+HEADER: :fit yes :imoutoptions -geometry 400 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
      % \author{Manuel Kirsch}
   \ovalbox{
   \begin{tikzpicture}[level/.style={sibling distance=60mm/#1}]
   \node [circle,draw] (z){$n$}
     child {node [circle,draw] (a) {$\frac{n}{2}$}
       child {node [circle,draw] (b) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}
	   child {node [circle,draw] (d) {$\frac{n}{2^k}$}}
	   child {node [circle,draw] (e) {$\frac{n}{2^k}$}}
	 } 
	 child {node {$\vdots$}}
       }
       child {node [circle,draw] (g) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}}
	 child {node {$\vdots$}}
       }
     }
     child {node [circle,draw] (j) {$\frac{n}{2}$}
       child {node [circle,draw] (k) {$\frac{n}{2^2}$}
	 child {node {$\vdots$}}
	 child {node {$\vdots$}}
       }
     child {node [circle,draw] (l) {$\frac{n}{2^2}$}
       child {node {$\vdots$}}
       child {node (c){$\vdots$}
	 child {node [circle,draw] (o) {$\frac{n}{2^k}$}}
	 child {node [circle,draw] (p) {$\frac{n}{2^k}$}
	   child [grow=right] {node (q) {$=$} edge from parent[draw=none]
	     child [grow=right] {node (q) {$O_{k = \lg n}(n)$} edge from parent[draw=none]
	       child [grow=up] {node (r) {$\vdots$} edge from parent[draw=none]
		 child [grow=up] {node (s) {$O_2(n)$} edge from parent[draw=none]
		   child [grow=up] {node (t) {$O_1(n)$} edge from parent[draw=none]
		     child [grow=up] {node (u) {$O_0(n)$} edge from parent[draw=none]}
		   }
		 }
	       }
	       child [grow=down] {node (v) {$O(n \cdot \lg n)$}edge from parent[draw=none]}
	     }
	   }
	 }
       }
     }
   };
   \path (a) -- (j) node [midway] {+};
   \path (b) -- (g) node [midway] {+};
   \path (k) -- (l) node [midway] {+};
   \path (k) -- (g) node [midway] {+};
   \path (d) -- (e) node [midway] {+};
   \path (o) -- (p) node [midway] {+};
   \path (o) -- (e) node (x) [midway] {$\cdots$}
     child [grow=down] {
       node (y) {$O\left(\displaystyle\sum_{i = 0}^k 2^i \cdot \frac{n}{2^i}\right)$}
       edge from parent[draw=none]
     };
   \path (q) -- (r) node [midway] {+};
   \path (s) -- (r) node [midway] {+};
   \path (s) -- (t) node [midway] {+};
   \path (s) -- (l) node [midway] {=};
   \path (t) -- (u) node [midway] {+};
   \path (z) -- (u) node [midway] {=};
   \path (j) -- (t) node [midway] {=};
   \path (y) -- (x) node [midway] {$\Downarrow$};
   \path (v) -- (y)
     node (w) [midway] {$O\left(\displaystyle\sum_{i = 0}^k n\right) = O(k \cdot n)$};
   \path (q) -- (v) node [midway] {=};
   \path (e) -- (x) node [midway] {+};
   \path (o) -- (x) node [midway] {+};
   \path (y) -- (w) node [midway] {$=$};
   \path (v) -- (w) node [midway] {$\Leftrightarrow$};
   \path (r) -- (c) node [midway] {$\cdots$};
   \end{tikzpicture}}
 #+END_src

* Mind map
[[http://www.texample.net/tikz/examples/computer-science-mindmap/][Mind map illustration from the TikZ gallery]] by Till Tantau


** Testing TikZ Mind map to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-mindmap.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{mindmap,trees}")
 #+HEADER: :fit yes :imoutoptions -geometry 600 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\begin{tikzpicture}
  \path[mindmap,concept color=black,text=white]
    node[concept] {Computer Science}
    [clockwise from=0]
    child[concept color=green!50!black] {
      node[concept] {practical}
      [clockwise from=90]
      child { node[concept] {algorithms} }
      child { node[concept] {data structures} }
      child { node[concept] {pro\-gramming languages} }
      child { node[concept] {software engineer\-ing} }
    }  
    child[concept color=blue] {
      node[concept] {applied}
      [clockwise from=-30]
      child { node[concept] {databases} }
      child { node[concept] {WWW} }
    }
    child[concept color=red] { node[concept] {technical} }
    child[concept color=orange] { node[concept] {theoretical} };
\end{tikzpicture}
 #+END_src

** Testing TikZ Mind map to SVG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-mindmap.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{mindmap,trees}")
 #+HEADER: :fit yes :imoutoptions -geometry 800 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\begin{tikzpicture}
  \path[mindmap,concept color=black,text=white]
    node[concept] {Computer Science}
    [clockwise from=0]
    child[concept color=green!50!black] {
      node[concept] {practical}
      [clockwise from=90]
      child { node[concept] {algorithms} }
      child { node[concept] {data structures} }
      child { node[concept] {pro\-gramming languages} }
      child { node[concept] {software engineer\-ing} }
    }  
    child[concept color=blue] {
      node[concept] {applied}
      [clockwise from=-30]
      child { node[concept] {databases} }
      child { node[concept] {WWW} }
    }
    child[concept color=red] { node[concept] {technical} }
    child[concept color=orange] { node[concept] {theoretical} };
\end{tikzpicture}
 #+END_src




* Filesystem tree
[[http://www.texample.net/tikz/examples/filesystem-tree/][Filesystem tree illustration from the TikZ gallery]] by Frantisek Burian


** Testing TikZ Filesystem tree to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-filesystem-tree.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{trees}")
 #+HEADER: :fit yes :imoutoptions -geometry 250 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\tikzstyle{every node}=[draw=black,thick,anchor=west]
\tikzstyle{selected}=[draw=red,fill=red!30]
\tikzstyle{optional}=[dashed,fill=gray!50]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
  \node {texmf}
    child { node {doc}}		
    child { node {fonts}}
    child { node {source}}
    child { node [selected] {tex}
      child { node {generic}}
      child { node [optional] {latex}}
      child { node {plain}}
    }
    child [missing] {}				
    child [missing] {}				
    child [missing] {}				
    child { node {texdoc}};
\end{tikzpicture}
 #+END_src



** Testing TikZ Filesystem tree to SVG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-filesystem-tree.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{trees}")
 #+HEADER: :fit yes :imoutoptions -geometry 800 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\tikzstyle{every node}=[draw=black,thick,anchor=west]
\tikzstyle{selected}=[draw=red,fill=red!30]
\tikzstyle{optional}=[dashed,fill=gray!50]
\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
  \node {texmf}
    child { node {doc}}		
    child { node {fonts}}
    child { node {source}}
    child { node [selected] {tex}
      child { node {generic}}
      child { node [optional] {latex}}
      child { node {plain}}
    }
    child [missing] {}				
    child [missing] {}				
    child [missing] {}				
    child { node {texdoc}};
\end{tikzpicture}
 #+END_src






* Sunflower
 [[http://www.texample.net/tikz/examples/phyllotaxy/][Sunflower from the TikZ gallery]] by  David Convent.
** Testing Sunfower to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-sunflower.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{calc}")
 #+HEADER: :fit yes :imoutoptions -geometry 250 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\def\nbrcircles {377}
\def\outerradius {30mm}
\def\deviation {.9}
\def\fudge {.62}

\newcounter{cumulArea}
\setcounter{cumulArea}{0}

\begin{document}
\begin{tikzpicture}[scale=.32]

\pgfmathsetmacro {\goldenRatio} {(1+sqrt(5))}
\pgfmathsetmacro {\meanArea}
      {pow(\outerradius * 10 / \nbrcircles, 2) * pi}
\pgfmathsetmacro {\minArea} {\meanArea * (1 - \deviation)}
\pgfmathsetmacro {\midArea} {\meanArea * (1 + \deviation) - \minArea}

\foreach \b in {0,...,\nbrcircles}{
    % mod() must be used in order to calculate the right angle.
    % otherwise, when \b is greater than 28 the angle is greater
    % than 16384 and an error is raised ('Dimension too large').
    % -- thx Tonio for this one.
    \pgfmathsetmacro{\angle}{mod(\goldenRatio * \b, 2) * 180}

    \pgfmathsetmacro{\sratio}{\b / \nbrcircles}
    \pgfmathsetmacro{\smArea}{\minArea + \sratio * \midArea}
    \pgfmathsetmacro{\smRadius}{sqrt(\smArea / pi) / 2 * \fudge}
    \addtocounter{cumulArea}{\smArea};

    \pgfmathparse{sqrt(\value{cumulArea} / pi) / 2}
    \fill[] (\angle:\pgfmathresult) circle [radius=\smRadius] ;
}  
\end{tikzpicture}
 #+END_src




** Testing Sunflower to SVG                                        :noexport:
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-sunflower.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{calc}")
 #+HEADER: :fit yes :imoutoptions -geometry 250 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\def\nbrcircles {377}
\def\outerradius {30mm}
\def\deviation {.9}
\def\fudge {.62}

\newcounter{cumulArea}
\setcounter{cumulArea}{0}

\begin{document}
\begin{tikzpicture}[scale=.32]

\pgfmathsetmacro {\goldenRatio} {(1+sqrt(5))}
\pgfmathsetmacro {\meanArea}
      {pow(\outerradius * 10 / \nbrcircles, 2) * pi}
\pgfmathsetmacro {\minArea} {\meanArea * (1 - \deviation)}
\pgfmathsetmacro {\midArea} {\meanArea * (1 + \deviation) - \minArea}

\foreach \b in {0,...,\nbrcircles}{
    % mod() must be used in order to calculate the right angle.
    % otherwise, when \b is greater than 28 the angle is greater
    % than 16384 and an error is raised ('Dimension too large').
    % -- thx Tonio for this one.
    \pgfmathsetmacro{\angle}{mod(\goldenRatio * \b, 2) * 180}

    \pgfmathsetmacro{\sratio}{\b / \nbrcircles}
    \pgfmathsetmacro{\smArea}{\minArea + \sratio * \midArea}
    \pgfmathsetmacro{\smRadius}{sqrt(\smArea / pi) / 2 * \fudge}
    \addtocounter{cumulArea}{\smArea};

    \pgfmathparse{sqrt(\value{cumulArea} / pi) / 2}
    \fill[] (\angle:\pgfmathresult) circle [radius=\smRadius] ;
}  
\end{tikzpicture}
 #+END_src



* Venn Diagram and magnifier

[[http://www.texample.net/tikz/examples/venn-diagram-with-magnifier/][From TikZ Gallery]] by Dennis Heidsiek

** Venn Diagram and magnifier to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-venndiagram-magnifier.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{spy}" "\\usepackage{verbatim}" "\\usepackage[active,tightpage]{preview}" "\\PreviewEnvironment{tikzpicture}")
 #+HEADER: :fit yes :imoutoptions -geometry 1000 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\def\firstcircle{(-2,0) circle (2.4)}
\def\secondcircle{(2,0) circle (2.4)}
\pgfmathparse{-(2.4^2-2^2)^0.5} % by pythagoras
\let\h\pgfmathresult % shortcut for further use
\def\thirdcircle{(0,\h) circle (0.2cm)}

\begin{tikzpicture}
 % Let's draw the scene (to magnify):
  \begin{scope}[spy using outlines=
      {magnification=16, size=8cm, connect spies, rounded corners}]
    
    % the boarder:
    \draw[thick, rounded corners] (-5,-4) rectangle (5,4);
    \draw (0,3.3) node[scale=2] {the universe of all jobs};
    
    % The transparency:
    \begin{scope}[fill opacity=0.5]
      \fill[red] \firstcircle;
      \fill[green] \secondcircle;
      \fill[blue] \thirdcircle;
    \end{scope}
    
    % letterings and missing pieces:
    \draw[align=center] \firstcircle node {what you\\would like\\to do};
    \draw[align=center] \secondcircle node {what you\\are able\\to do};
    \draw \thirdcircle
      (0,-2.3) node[align=center] {em-\\ployment\\you get paid enough for};
    \fill (0,\h+0.12) circle (0.005)
        node[scale=0.11, align=center] {perfect\\job};
    
    % now we can draw the magnifying glass:
    \spy [red] on (0,\h) in node [left] at (13.25,0);
  \end{scope}
\end{tikzpicture}
 #+END_src





** Venn Diagram and magnifier to SVG :noexport:
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-venndiagram-magnifier.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{spy}" "\\usepackage{verbatim}" "\\usepackage[active,tightpage]{preview}" "\\PreviewEnvironment{tikzpicture}")
 #+HEADER: :fit yes :imoutoptions -geometry 250 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\def\firstcircle{(-2,0) circle (2.4)}
\def\secondcircle{(2,0) circle (2.4)}
\pgfmathparse{-(2.4^2-2^2)^0.5} % by pythagoras
\let\h\pgfmathresult % shortcut for further use
\def\thirdcircle{(0,\h) circle (0.2cm)}

\begin{tikzpicture}
 % Let's draw the scene (to magnify):
  \begin{scope}[spy using outlines=
      {magnification=16, size=8cm, connect spies, rounded corners}]
    
    % the boarder:
    \draw[thick, rounded corners] (-5,-4) rectangle (5,4);
    \draw (0,3.3) node[scale=2] {the universe of all jobs};
    
    % The transparency:
    \begin{scope}[fill opacity=0.5]
      \fill[red] \firstcircle;
      \fill[green] \secondcircle;
      \fill[blue] \thirdcircle;
    \end{scope}
    
    % letterings and missing pieces:
    \draw[align=center] \firstcircle node {what you\\would like\\to do};
    \draw[align=center] \secondcircle node {what you\\are able\\to do};
    \draw \thirdcircle
      (0,-2.3) node[align=center] {em-\\ployment\\you get paid enough for};
    \fill (0,\h+0.12) circle (0.005)
        node[scale=0.11, align=center] {perfect\\job};
    
    % now we can draw the magnifying glass:
    \spy [red] on (0,\h) in node [left] at (13.25,0);
  \end{scope}
\end{tikzpicture}
 #+END_src

* Lindenmayer systems
[[http://www.texample.net/tikz/examples/lindenmayer-systems/][From the TikZ gallery]] by Stefan Kottwitz


** Testing Lindenmayer systems to PNG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-lindenmayer.png :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{lindenmayersystems}" "\\usetikzlibrary{shadings}")
 #+HEADER: :fit yes :imoutoptions -geometry 600 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\pgfdeclarelindenmayersystem{Koch curve}{
  \rule{F -> F-F++F-F}}
\pgfdeclarelindenmayersystem{Sierpinski triangle}{
  \rule{F -> G-F-G}
  \rule{G -> F+G+F}}
\pgfdeclarelindenmayersystem{Fractal plant}{
  \rule{X -> F-[[X]+X]+F[+FX]-X}
  \rule{F -> FF}}
\pgfdeclarelindenmayersystem{Hilbert curve}{
  \rule{L -> +RF-LFL-FR+}
  \rule{R -> -LF+RFR+FL-}}

\begin{tabular}{cc}
\begin{tikzpicture}
\shadedraw[shading=color wheel] 
[l-system={Koch curve, step=2pt, angle=60, axiom=F++F++F, order=4}]
lindenmayer system -- cycle;
\end{tikzpicture}
&
\begin{tikzpicture}
\shadedraw [top color=white, bottom color=blue!80, draw=blue!80!black]
[l-system={Sierpinski triangle, step=2pt, angle=60, axiom=F, order=8}]
lindenmayer system -- cycle;
\end{tikzpicture}
\\
\begin{tikzpicture}
    \shadedraw [bottom color=white, top color=red!80, draw=red!80!black]
    [l-system={Hilbert curve, axiom=L, order=5, step=8pt, angle=90}]
    lindenmayer system; 
\end{tikzpicture}
&
\begin{tikzpicture}
    \draw [green!50!black, rotate=90]
    [l-system={Fractal plant, axiom=X, order=6, step=2pt, angle=25}]
    lindenmayer system; 
\end{tikzpicture}
\end{tabular}
 #+END_src


** Testing Lindenmayer systems to SVG
:PROPERTIES:
  :reveal_background: #888888
:END:

 #+HEADER: :file img/test-latex-tikz-lindenmayer.svg :imagemagick yes
 #+HEADER: :results output :headers '("\\usepackage{tikz}" "\\usetikzlibrary{lindenmayersystems}" "\\usetikzlibrary{shadings}")
 #+HEADER: :fit yes :imoutoptions -geometry 600 :iminoptions -density 600
 #+BEGIN_src latex :results raw :exports results
\pgfdeclarelindenmayersystem{Koch curve}{
  \rule{F -> F-F++F-F}}
\pgfdeclarelindenmayersystem{Sierpinski triangle}{
  \rule{F -> G-F-G}
  \rule{G -> F+G+F}}
\pgfdeclarelindenmayersystem{Fractal plant}{
  \rule{X -> F-[[X]+X]+F[+FX]-X}
  \rule{F -> FF}}
\pgfdeclarelindenmayersystem{Hilbert curve}{
  \rule{L -> +RF-LFL-FR+}
  \rule{R -> -LF+RFR+FL-}}

\begin{tabular}{cc}
\begin{tikzpicture}
\shadedraw[shading=color wheel] 
[l-system={Koch curve, step=2pt, angle=60, axiom=F++F++F, order=4}]
lindenmayer system -- cycle;
\end{tikzpicture}
&
\begin{tikzpicture}
\shadedraw [top color=white, bottom color=blue!80, draw=blue!80!black]
[l-system={Sierpinski triangle, step=2pt, angle=60, axiom=F, order=8}]
lindenmayer system -- cycle;
\end{tikzpicture}
\\
\begin{tikzpicture}
    \shadedraw [bottom color=white, top color=red!80, draw=red!80!black]
    [l-system={Hilbert curve, axiom=L, order=5, step=8pt, angle=90}]
    lindenmayer system; 
\end{tikzpicture}
&
\begin{tikzpicture}
    \draw [green!50!black, rotate=90]
    [l-system={Fractal plant, axiom=X, order=6, step=2pt, angle=25}]
    lindenmayer system; 
\end{tikzpicture}
\end{tabular}
 #+END_src

 #+RESULTS:
 [[file:img/test-latex-tikz-lindenmayer.svg]]


* TODO Investigate errors

Some examples do not work, especially to SVG. Tex4ht seems to be the
culprit. However, generating a pdf and using [[https://packages.debian.org/sid/pdf2svg][pdf2svg]] would work, so
why does babel latex not do that ?
** Option 1 : Fixing ob-latex.el
So that it would call the best latex backend and use pdf2svg if available

** Option 2 : workaround
- [[https://www.reddit.com/r/emacs/comments/akqoho/org_babel_compile_latex_using_xelatex/efa4w5n?utm_source=share&utm_medium=web2x][advise org-babel-latex-tex-to-pdf]]
- fiddle with [[http://www.imagemagick.org/discourse-server/viewtopic.php?t=7856][delegates.xml]] of Imagemagick
*** latex backend
for =xelatex=, it would be :
#+BEGIN_SRC lisp
(advice-add 'org-babel-latex-tex-to-pdf :before
            (lambda (file)
              (with-temp-buffer
                (insert-file-contents file)
                (beginning-of-buffer)
                (insert "% xelatex\n")
                (write-file file))))
#+END_SRC
*** delegates.xml 
- decode : pdf
- encode : svg
→ [[https://packages.debian.org/sid/pdf2svg][pdf2svg]]
